import {
  GetProductsForIngredient,
  GetRecipes,
} from "./supporting-files/data-access";
import {
  NutrientFact,
  Recipe,
  RecipeLineItem,
  UnitOfMeasure,
} from "./supporting-files/models";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";
import {
  ConvertUnitsToBaseUnit,
  ConvertUnitsToDefaultBaseUnit,
  GetCheapestProduct,
  RecipeSummary,
} from "./supporting-files/custom-helpers";
import { SumUnitsOfMeasure } from "./supporting-files/helpers";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
let recipeSummary: RecipeSummary = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

recipeData.forEach((recipe: Recipe) => {
  const recipeName = recipe.recipeName;
  let nutrientsAtCheapestCost: { [key: string]: NutrientFact } = {};

  // define dictionary for convenient search by key - key: ingredient name, value: rates
  let ingredientNameRates = {};

  // define dictionary convenient search by key - key: ingredient name, value: nutrient facts
  let ingredientNameNutrientFacts = {};

  // get sum of all line items to use when calculating the rate of each ingredient
  const sumOflineItems = recipe.lineItems
    .map((item) => ConvertUnitsToDefaultBaseUnit(item.unitOfMeasure))
    .reduce((result: UnitOfMeasure, value: UnitOfMeasure) => {
      return SumUnitsOfMeasure(result, value);
    });

  /*
   * Gets the rates of each ingredient in recipe
   * For it, we need to make the units of all ingredients same.
   * Let's use default base unit UoMName.grams and UoMType.mass
   * So I am going to use Base Unit of each other.
   */
  recipe.lineItems.forEach((item: RecipeLineItem) => {
    const lintItem = ConvertUnitsToDefaultBaseUnit(item.unitOfMeasure);
    ingredientNameRates = {
      ...ingredientNameRates,
      [item.ingredient.ingredientName]:
        lintItem.uomAmount / sumOflineItems.uomAmount,
    };
  });

  const results = recipe.lineItems.map((item: RecipeLineItem) => {
    // get products from ingredient name
    const products = GetProductsForIngredient(item.ingredient);

    // next we need to get the product which has the cheapest supplier product.
    const cheapestProduct = GetCheapestProduct(products);

    /*
     * covert recipelineitem unit to base unit
     * We need to covert the recipe line item's unit to the supplier product's base unit
     * But we can't do it directly because "cup" can't be converted to "kilogram" directly.
     * So I am going to use Base Unit of each other.
     */
    const convertedUnit = ConvertUnitsToBaseUnit(item.unitOfMeasure);

    // complete key & value pair of ingredient name & nutrient facts
    ingredientNameNutrientFacts = {
      ...ingredientNameNutrientFacts,
      [item.ingredient.ingredientName]: cheapestProduct.nutrientFacts,
    };

    return convertedUnit.uomAmount * cheapestProduct.cheapestCostPerBaseUnit;
  });

  const cheapestCost = results.reduce((prev, curr) => {
    return prev + curr;
  }, 0);

  // complete nutrientsAtCheapestCost by using two dictionaries - Rates & Facts.
  Object.keys(ingredientNameNutrientFacts).forEach((key: string) => {
    ingredientNameNutrientFacts[key].forEach((fact: NutrientFact) => {
      if (fact.nutrientName in nutrientsAtCheapestCost) {
        const prev = nutrientsAtCheapestCost[fact.nutrientName];
        nutrientsAtCheapestCost[fact.nutrientName] = {
          ...prev,
          quantityAmount: {
            ...prev.quantityAmount,
            uomAmount:
              prev.quantityAmount.uomAmount +
              fact.quantityAmount.uomAmount * ingredientNameRates[key],
          },
        };
      } else {
        nutrientsAtCheapestCost[fact.nutrientName] = {
          nutrientName: fact.nutrientName,
          quantityAmount: {
            ...fact.quantityAmount,
            uomAmount: fact.quantityAmount.uomAmount * ingredientNameRates[key],
          },
          quantityPer: fact.quantityPer,
        };
      }
    });
  });

  recipeSummary = {
    ...recipeSummary,
    [recipeName]: {
      cheapestCost: cheapestCost,
      nutrientsAtCheapestCost: nutrientsAtCheapestCost,
    },
  };
});

Object.keys(recipeSummary).forEach((key: string) => {
  console.log(recipeSummary[key].cheapestCost);
  Object.keys(recipeSummary[key].nutrientsAtCheapestCost).forEach(
    (key1: string) => {
      console.log(key1, recipeSummary[key].nutrientsAtCheapestCost[key1]);
    }
  );
});

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
