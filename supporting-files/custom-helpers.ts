import { GetBaseUoM, NutrientBaseUoM } from "./data-access";
import {
  ConvertUnits,
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
} from "./helpers";
import {
  UnitOfMeasure,
  NutrientFact,
  Product,
  SupplierProduct,
  UoMName,
  UoMType,
} from "./models";

export interface RecipeSummary {
  [key: string]: {
    cheapestCost: number;
    nutrientsAtCheapestCost: {
      [key: string]: NutrientFact;
    };
  };
}

export interface ProductWithCheapestCost extends Product {
  cheapestCostPerBaseUnit: number;
}

/*
 * Converts from one unit of measure to base unit of measure
 */
export function ConvertUnitsToBaseUnit(unit: UnitOfMeasure): UnitOfMeasure {
  const baseUnitOfMeasure = GetBaseUoM(unit.uomType);

  return ConvertUnits(
    unit,
    baseUnitOfMeasure.uomName,
    baseUnitOfMeasure.uomType
  );
}


/*
 * Converts from one unit of measure to default base unit of measure (UoMName.grams and UoMType.mass)
 */
export function ConvertUnitsToDefaultBaseUnit(unit: UnitOfMeasure): UnitOfMeasure {
    const baseUnitOfMeasure = ConvertUnitsToBaseUnit(unit);
  
    return ConvertUnits(
        baseUnitOfMeasure,
      UoMName.grams,
      UoMType.mass
    );
  }


/*
 * Gets the product which has the cheapest supplier product.
 */
export function GetCheapestProduct(
  products: Product[]
): ProductWithCheapestCost {
  const arrayProductWithCheapestCost: ProductWithCheapestCost[] = products.map(
    (product: Product) => {
      const cheapestCostPerBaseUnit = Math.min.apply(
        Math,
        product.supplierProducts.map((supplierProduct: SupplierProduct) =>
          GetCostPerBaseUnit(supplierProduct)
        )
      );

      const nutrientFacts = product.nutrientFacts.map((fact: NutrientFact) => {
        const tempNutrient = GetNutrientFactInBaseUnits(fact);
        return {
          nutrientName: tempNutrient.nutrientName,
          quantityAmount: {
            ...tempNutrient.quantityAmount,
            uomAmount:
              (tempNutrient.quantityAmount.uomAmount /
                tempNutrient.quantityPer.uomAmount) *
              NutrientBaseUoM.uomAmount,
          },
          quantityPer: NutrientBaseUoM,
        };
      });

      return {
        ...product,
        nutrientFacts: nutrientFacts,
        cheapestCostPerBaseUnit,
      };
    }
  );

  const cheapestProduct = arrayProductWithCheapestCost.reduce((prev, curr) => {
    return curr.cheapestCostPerBaseUnit < prev.cheapestCostPerBaseUnit
      ? curr
      : prev;
  });

  return cheapestProduct;
}
